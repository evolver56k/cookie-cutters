# Open Cookies!

Free to use plans for 3d printed cookie cutters. 

OpenSCAD files generated from Inkscape via [OpenSCAD cookie cutter file output](https://inkscape.org/~arpruss/%E2%98%85openscad-cookie-cutter-file-output)

# LICENSE

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
